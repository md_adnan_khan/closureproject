function counterFactory() {
    let counter = 0;
    return {
        increment: function() {
            return counter + 1;
        },
        decrement: function() {
            return counter - 1;
        },
    };
}

// let object = counterFactory();
// let increm = object.increment();
// console.log(increm);
module.exports.counterFactory = counterFactory;