function cacheFunction(callback) {

    const cache = {
        name: "Thor",
        location: "Asguard",
    };
    
    return function invokeCallBack(argumentObject) {
        
        for(let key in argumentObject) {
            if(key in cache) { // if argument is already seen in cache then return cache
                return cache;
            } else { //Put keys and values in cache to track all the arguments
                cache[key] = argumentObject[key];
            }
        }
        return callback(cache);
    }
}

module.exports.cacheFunction = cacheFunction;