const {cacheFunction} = require('../cacheFunction.cjs');

let argumentObject = {
    name: "Batman",
    age: 34,
}

function callBackFuction(cache) {
    console.log(`cb is invoked once for a given set of arguments:`)
    return cache;
}

let cacheObject = cacheFunction(callBackFuction);

let value = cacheObject(argumentObject);
console.log(value);
