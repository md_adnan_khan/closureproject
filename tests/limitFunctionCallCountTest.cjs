const {limitFunctionCallCount} = require('../limitFunctionCallCount.cjs');

let nTimes = 3;

function functionToBeInvoked(count) { //callback function
    count++;
    return `I am callback and I am being invoked ${count} times`;
}

let callNTimes = limitFunctionCallCount(functionToBeInvoked, nTimes);

//Now calling 'nTimes' or more than 'nTimes'
console.log(callNTimes());
console.log(callNTimes());
console.log(callNTimes());
console.log(callNTimes());
console.log(callNTimes());
console.log(callNTimes());
console.log(callNTimes());