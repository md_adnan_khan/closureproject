const {counterFactory} = require('../counterFactory.cjs');

let counterFactoryObject = counterFactory();
let increment = counterFactoryObject.increment();
console.log(`Increment: ${increment}`);

let decrement = counterFactoryObject.decrement();
console.log(`Decrement: ${decrement}`);

