function limitFunctionCallCount(callback, nTimes) {

    return function invokeCallback() {
        if(nTimes > 0) {
            nTimes--;
            return callback(nTimes);
        } else {
            return null;
        }
    };
}

module.exports.limitFunctionCallCount = limitFunctionCallCount;